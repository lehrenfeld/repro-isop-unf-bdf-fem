# Reproduction material for "ISOPARAMETRIC UNFITTED BDF – FINITE ELEMENT METHOD FOR PDES ON EVOLVING DOMAINS" by Y. Lou and C. Lehrenfeld

In this repo we gather reproduction material for the above mentioned paper and some additional material. 

## Content:
In this section we simply list files and directories in alphabetical ordering, starting with the directories:

* directory `binder`: 
In the `binder` directory, the `Dockerfile` is stored that allows to run convergence studies (not only) in the binder cloud. You can start it from here [ `convstudies.ipynb`: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Frepro-isop-unf-bdf-fem.git/master?filepath=solver%2Fconvstudies.ipynb), `demo.ipynb`: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Frepro-isop-unf-bdf-fem.git/master?filepath=solver%2Fdemo.ipynb) ]. Note that this way you can directly and platform-indepedently start right away. Computing ressources in the binder cloud will be limited and may restrict the usage to computations on moderate meshes only. However, for some first results and explanation this is very useful as it circumvents the need for additional installations.

* directory `data` : 
In the directory `data`, we store some convergence tables, especially those used in the paper. These can be visualized with the file provided in `plots`, see below.

* directory `demo` : 
In `demo`, we provide documented jupyter notebook files that briefly explain how one simulation run for one fixed step size and one mesh is carried in the studies in the paper.
To run the files you will need a proper installation of `ngsxfem` and `jupyter`, see the section "Setup ngsxfem to run the examples" below.

* directory `plots` :
In the directory `plots` we provide a latex file that allows you (assuming a proper latex installation) to generate plots as in the paper. You will only have to adapt the filename in the tex-file to point to the correct data. 

* directory `solver` :
here, we've put the files that were actually used to generate the results of the paper. 

* directory `examples` :
Here, we've put the example parameters and functions that are used in the convergence studies and/or the demo files. These are:
  * kite.py: The one-domain example used in Section 7.1 in the paper
  * intfprob.py: The two-domain interface problem used in Section 7.2 in the paper
  
You can easily define your own example by copying these files and changing the functions and parameters.

* file `driver.py`
This file can be called from python to let a convergence study run. This will run the study and store the results in csv files in the same directory. 
To run the file you will need a proper installation of `ngsxfem`, see the section "Setup ngsxfem to run the examples" below.
The file should be called as:
```
python3 driver.py
```
There are several options that can be set, e.g. the example, the range of refinements in the convergence study, etc.. To see all possible options call
```
python3 driver.py --help
```

## Setup ngsxfem to run the examples.
For reproducibility we recommend to use the Docker image of ngsxfem also used in `binder/Dockerfile`, but you may also follow the installation instructions of [`ngsxfem`](http://github.com/ngsxfem/ngsxfem) for a local installation. 
    
