## run with e.g.:
## python moving_domain.py --example kite --k 2 --r 2 --no-vtkout -rx 3 5 -rt 0 3

"""
"""
# ------------------------------ LOAD LIBRARIES -------------------------------
import argparse

parser = argparse.ArgumentParser(description='Solve isoparam. unfitted BDF time steppting.')
parser.add_argument('-e','--example', default="kite", help='example (name of example file), default: kite')
parser.add_argument('-k','--k', metavar='N', type=int, default=2, help='k (FE order), default: 2')
parser.add_argument('-q','--q', metavar='N', type=int, default=-1, help='q (geom order), default: k')
parser.add_argument('-r','--r', metavar='N', type=int, default=2, help='order of BDF scheme, default: 2')
parser.add_argument('-cg','--c_gamma', metavar='N', type=float, default=1, help='c_gamma (Ghost penalty), default: 1')
parser.add_argument('-ln','--lambda_N', metavar='N', type=float, default=40, help='lambda_N (Nitsche penalty), default: 40')
parser.add_argument('--vtkout', dest='vtkout', action='store_true', help='enable VTKOutput, default: disabled')
parser.add_argument('--no-vtkout', dest='vtkout', action='store_false', help='disable VTKOutput, default: disabled')
parser.add_argument('-n', '--nt', metavar='N', type=int, default=4, help='number of threads, default: 4')
parser.add_argument('-rx','--rx', nargs='+', help='Range for space refinements (incl. last index), default: 0 2', default = ['0','2'])
parser.add_argument('-rt','--rt', nargs='+', help='Range for time refinements (incl. last index), default: 0 2', default = ['0','2'])
parser.add_argument('-g','--gui', dest='gui', action='store_true', help='enable netgen gui, default: disabled')
parser.add_argument('-ng','--no-gui', dest='gui', action='store_false', help='disable netgen gui, default: disabled')
parser.add_argument('-d','--write-data', dest='write-data', action='store_true', help='write results to data files')
parser.add_argument('-nd','--dont-write-data', dest='dont-write-data', action='store_true', help='don\'t write results to data files')

parser.set_defaults(vtkout=False)
parser.set_defaults(gui=False)
args = parser.parse_args()
options = vars(args)

if options["gui"]:
    from netgen import gui

rLt = [int(w) for w in options["rt"]]
rLx = [int(w) for w in options["rx"]]
if len(rLt) != 2:
    raise Exception("length of 'rt' must be two!")
if len(rLx) != 2:
    raise Exception("length of 'rx' must be two!")
Lts = list(range(rLt[0],rLt[1]+1))
Lxs = list(range(rLx[0],rLx[1]+1))

print("time refinements:", Lts)
print("space refinements:", Lxs)

q = options["q"]
if q == -1:
    q = options["k"]

print("Example:", options["example"])
print("Using k = " + str(options["k"]) + ", q = "+str(q)+", BDF-"+str(options["r"]))


# Problem Data
# Initial condition and right-hand side (read from imported data)

# ----------------------------------- MAIN ------------------------------------
from ngsolve import *
SetNumThreads(options["nt"])

from solve import *

errors = Solve(options,Lxs=Lxs,Lts=Lts)
print(errors)


from numpy import savetxt, concatenate, array, c_
Lts0 = [0]; Lts0.extend(Lts)
basefilename = options["example"]+"_P"+str(options["k"])+"BDF"+str(options["r"])

if not options["dont-write-data"]:
    if not options["write-data"]:
        input("continue (ENTER) to write table to data files (prefix=\""+basefilename+"\")")
    
    filenamestr = basefilename +"_cgam_"+str(options["c_gamma"])
    if options["example"] == "intfprob":
        filenamestr += "_lamN_"+str(options["lambda_N"])
    for et in errors:
        output_table = c_[Lts0,c_[Lxs,errors[et]].transpose()].transpose()
        savetxt(filenamestr +"_error_"+et+".data", output_table, delimiter = "\t")
        savetxt(filenamestr +"_error_"+et+"t.data", output_table.transpose(), delimiter = "\t")
    
