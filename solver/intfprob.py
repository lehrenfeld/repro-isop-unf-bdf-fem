# Two-phase interface

from ngsolve import *
from math import pi, ceil

maxh = 1/2

#nu = 1
T = 1/2
dt_0 = T

alpha = [10,20]
beta = [2,1]

lowerleft = [-0.5,0.0]
upperright = [1.5,2]

R0 = 1/3
aa = 1.1569
bb = -8.1621

visualization_min = -1
visualization_max = 1

def rho_function(t):
    return 1/(4*pi)*sin(2*pi*t)
def velocity_function(t):
    return CoefficientFunction( (1/2*cos(2*pi*t), 0.0) )
def velmax():
    return 1/2
def div_w_function(t):
    return CoefficientFunction( 0.0 )
def r_function(x,y,t):
    return sqrt((x-rho_function(t)-1/2)**2+(y-1)**2)
def R(t):
    return R0*exp(0.0)
def levelset_function(t):
    return r_function(x,y,t) - R(t)
    
def u_a(s):
    return aa + bb*s*s
def u_b(s):
    return cos(pi*s)
    
def solution_a(t):
    return sin(pi*t)*u_a(r_function(x,y,t))
def solution_b(t):
    return sin(pi*t)*u_b(r_function(x,y,t))
    
def grad_a(t):
    return CoefficientFunction((solution_a(t).Diff(x),solution_a(t).Diff(y)))
    #return 2*bb*sin(pi*t)*r_function(x,y,t) * CoefficientFunction( (r_x(x,y,t),r_y(x,y,t)) )
def grad_b(t):
    return CoefficientFunction((solution_b(t).Diff(x),solution_b(t).Diff(y)))
    #return -pi*sin(pi*t)*sin(pi*r_function(x,y,t)) * CoefficientFunction( (r_x(x,y,t),r_y(x,y,t)) )

def rhs_a(t):
    return -alpha[0]*(solution_a(t).Diff(x).Diff(x) + solution_a(t).Diff(y).Diff(y)) + solution_a(t).Diff(t) + InnerProduct(velocity_function(t),grad_a(t))
    #return pi*cos(pi*t)*u_a(r_function(x,y,t)) - alpha[0]*sin(pi*t)*2*bb - alpha[0]*sin(pi*t)*2*bb
def rhs_b(t):
    return -alpha[1]*(solution_b(t).Diff(x).Diff(x) + solution_b(t).Diff(y).Diff(y)) + solution_b(t).Diff(t) + InnerProduct(velocity_function(t),grad_b(t))
    #return pi*cos(pi*t)*cos(pi*r_function(x,y,t)) + alpha[1]*sin(pi*t)*pi*pi*cos(pi*r_function(x,y,t)) + alpha[1]*pi*sin(pi*t)*sin(pi*r_function(x,y,t))/r_function(x,y,t)

def solution_function(t):
    return [solution_a(t), solution_b(t)]

def grad_solution_function(t):
    return [grad_a(t), grad_b(t)]

def rhs_function(t):
    return [rhs_a(t), rhs_b(t)]
